﻿namespace Common.DTO
{
    public enum TaskStatesDTO
    {
        Canceled = 0,
        Started,
        Finished,
        Stopped
    }
}
