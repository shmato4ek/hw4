﻿using HW4.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace HW4.DAL.EF
{
    public class ProjectsContext : DbContext
    {
        public ProjectsContext() { }
        public ProjectsContext(DbContextOptions<ProjectsContext> options) : base(options) { }
        public DbSet<ProjectEntity> Projects { get; set; }
        public DbSet<TaskEntity> Tasks { get; set; }
        public DbSet<TeamEntity> Teams { get; set; }
        public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();
            modelBuilder.Seed();
            base.OnModelCreating(modelBuilder);
        }
    }
}
