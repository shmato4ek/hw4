﻿using HW4.DAL.Entities;
using HW4.DAL.Repository;
using System.Threading.Tasks;

namespace HW4.DAL.UOW
{
    public interface IUnitOfWork
    {
        IRepository<ProjectEntity> Projects { get; }
        IRepository<UserEntity> Users { get; }
        IRepository<TaskEntity> Tasks { get; }
        IRepository<TeamEntity> Teams { get; }
        Task Save();
    }
}
