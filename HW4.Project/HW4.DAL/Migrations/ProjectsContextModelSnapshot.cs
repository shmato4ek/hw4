﻿// <auto-generated />
using System;
using HW4.DAL.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace HW4.DAL.Migrations
{
    [DbContext(typeof(ProjectsContext))]
    partial class ProjectsContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.13")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("HW4.DAL.Entities.ProjectEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AuthorId")
                        .HasColumnType("int");

                    b.Property<DateTime?>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValue(new DateTime(2022, 1, 22, 1, 12, 42, 213, DateTimeKind.Local).AddTicks(2628));

                    b.Property<DateTime?>("Deadline")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("AuthorId");

                    b.HasIndex("TeamId");

                    b.ToTable("Projects");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AuthorId = 1,
                            CreatedAt = new DateTime(2021, 10, 11, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2022, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Platform for listening and saving user`s favourite music",
                            Name = "Music platform",
                            TeamId = 1
                        },
                        new
                        {
                            Id = 2,
                            AuthorId = 2,
                            CreatedAt = new DateTime(2022, 11, 2, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2022, 2, 21, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Web-site, where users can know more informayion about our company",
                            Name = "Web-site for company",
                            TeamId = 2
                        },
                        new
                        {
                            Id = 3,
                            AuthorId = 3,
                            CreatedAt = new DateTime(2021, 9, 21, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Creating new campus",
                            Name = "KPI Campus",
                            TeamId = 2
                        },
                        new
                        {
                            Id = 4,
                            AuthorId = 4,
                            CreatedAt = new DateTime(2021, 7, 12, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2021, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Site for study controll, based on Moodle",
                            Name = "Moodle-based site",
                            TeamId = 3
                        },
                        new
                        {
                            Id = 5,
                            AuthorId = 5,
                            CreatedAt = new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Deadline = new DateTime(2022, 3, 4, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "New shop need web-site",
                            Name = "Web-site for new shop",
                            TeamId = 3
                        });
                });

            modelBuilder.Entity("HW4.DAL.Entities.TaskEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValue(new DateTime(2022, 1, 22, 1, 12, 42, 225, DateTimeKind.Local).AddTicks(6221));

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("FinishedAtDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("PerformerId")
                        .HasColumnType("int");

                    b.Property<int>("ProjectId")
                        .HasColumnType("int");

                    b.Property<int>("State")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasDefaultValue(1);

                    b.HasKey("Id");

                    b.HasIndex("PerformerId");

                    b.HasIndex("ProjectId");

                    b.ToTable("Tasks");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 10, 11, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Write back for the platform",
                            Name = "Writing back",
                            PerformerId = 5,
                            ProjectId = 1,
                            State = 1
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 1, 8, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Writing back and front for new web-site",
                            Name = "Create simple web-site for company",
                            PerformerId = 4,
                            ProjectId = 2,
                            State = 1
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2022, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Get all information about students",
                            FinishedAtDate = new DateTime(2021, 12, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Find data",
                            PerformerId = 3,
                            ProjectId = 3,
                            State = 2
                        },
                        new
                        {
                            Id = 4,
                            CreatedAt = new DateTime(2021, 10, 22, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Write back for campus",
                            FinishedAtDate = new DateTime(2021, 12, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Write back",
                            PerformerId = 3,
                            ProjectId = 3,
                            State = 0
                        },
                        new
                        {
                            Id = 5,
                            CreatedAt = new DateTime(2021, 9, 21, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Write front for campus",
                            FinishedAtDate = new DateTime(2021, 11, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Write front",
                            PerformerId = 2,
                            ProjectId = 3,
                            State = 0
                        },
                        new
                        {
                            Id = 6,
                            CreatedAt = new DateTime(2021, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Think about realization",
                            FinishedAtDate = new DateTime(2021, 12, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Create idea",
                            PerformerId = 2,
                            ProjectId = 4,
                            State = 2
                        },
                        new
                        {
                            Id = 7,
                            CreatedAt = new DateTime(2022, 1, 9, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Meet the customer and talk with him about the project",
                            Name = "Talk with customer",
                            PerformerId = 1,
                            ProjectId = 5,
                            State = 1
                        },
                        new
                        {
                            Id = 8,
                            CreatedAt = new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Description = "Implement customer`s thinks",
                            FinishedAtDate = new DateTime(2022, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Implementing",
                            PerformerId = 1,
                            ProjectId = 5,
                            State = 0
                        });
                });

            modelBuilder.Entity("HW4.DAL.Entities.TeamEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValue(new DateTime(2022, 1, 22, 1, 12, 42, 226, DateTimeKind.Local).AddTicks(972));

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Teams");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 3, 18, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Team A"
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 3, 19, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Team B"
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2021, 6, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Team C"
                        });
                });

            modelBuilder.Entity("HW4.DAL.Entities.UserEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("Birthday")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("RegisteredAt")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValue(new DateTime(2022, 1, 22, 1, 12, 42, 226, DateTimeKind.Local).AddTicks(2829));

                    b.Property<int?>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TeamId");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Birthday = new DateTime(2000, 3, 25, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Email = "anders78@gmail.com",
                            FirstName = "Alaya",
                            LastName = "Andersen",
                            RegisteredAt = new DateTime(2021, 12, 28, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            TeamId = 1
                        },
                        new
                        {
                            Id = 2,
                            Birthday = new DateTime(1992, 7, 4, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Email = "kubakey92@gmail.com",
                            FirstName = "Kuba",
                            LastName = "Kay",
                            RegisteredAt = new DateTime(2021, 12, 22, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            TeamId = 2
                        },
                        new
                        {
                            Id = 3,
                            Birthday = new DateTime(2013, 3, 11, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Email = "jevon.melia@gmail.com",
                            FirstName = "Jevon",
                            LastName = "Melia",
                            RegisteredAt = new DateTime(2017, 10, 18, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            TeamId = 2
                        },
                        new
                        {
                            Id = 4,
                            Birthday = new DateTime(2002, 10, 21, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Email = "f0st3rida@gmai.com",
                            FirstName = "Ida",
                            LastName = "Foster",
                            RegisteredAt = new DateTime(2019, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            TeamId = 3
                        },
                        new
                        {
                            Id = 5,
                            Birthday = new DateTime(2010, 8, 25, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Email = "cadence12.com",
                            FirstName = "Cadence",
                            LastName = "Watson",
                            RegisteredAt = new DateTime(2019, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            TeamId = 3
                        });
                });

            modelBuilder.Entity("HW4.DAL.Entities.ProjectEntity", b =>
                {
                    b.HasOne("HW4.DAL.Entities.UserEntity", "Author")
                        .WithMany("Projects")
                        .HasForeignKey("AuthorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("HW4.DAL.Entities.TeamEntity", "Team")
                        .WithMany("Projects")
                        .HasForeignKey("TeamId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Author");

                    b.Navigation("Team");
                });

            modelBuilder.Entity("HW4.DAL.Entities.TaskEntity", b =>
                {
                    b.HasOne("HW4.DAL.Entities.UserEntity", "Performer")
                        .WithMany("Tasks")
                        .HasForeignKey("PerformerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("HW4.DAL.Entities.ProjectEntity", "Project")
                        .WithMany("Tasks")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Performer");

                    b.Navigation("Project");
                });

            modelBuilder.Entity("HW4.DAL.Entities.UserEntity", b =>
                {
                    b.HasOne("HW4.DAL.Entities.TeamEntity", "Team")
                        .WithMany("Users")
                        .HasForeignKey("TeamId");

                    b.Navigation("Team");
                });

            modelBuilder.Entity("HW4.DAL.Entities.ProjectEntity", b =>
                {
                    b.Navigation("Tasks");
                });

            modelBuilder.Entity("HW4.DAL.Entities.TeamEntity", b =>
                {
                    b.Navigation("Projects");

                    b.Navigation("Users");
                });

            modelBuilder.Entity("HW4.DAL.Entities.UserEntity", b =>
                {
                    b.Navigation("Projects");

                    b.Navigation("Tasks");
                });
#pragma warning restore 612, 618
        }
    }
}
