﻿using HW4.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HW4.DAL.Entities
{
    [Table("Users")]
    public class UserEntity : BaseEntity
    {
        public UserEntity()
        {
            Projects = new List<ProjectEntity>();
            Tasks = new List<TaskEntity>();
        }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime? RegisteredAt { get; set; }
        public DateTime? Birthday { get; set; }
        public TeamEntity Team { get; set; }
        public ICollection<ProjectEntity> Projects { get; set; }
        public ICollection<TaskEntity> Tasks { get; set; }
    }
}
