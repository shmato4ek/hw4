﻿using HW4.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HW4.DAL.Entities
{
    [Table("Teams")]
    public class TeamEntity : BaseEntity
    {
        public TeamEntity()
        {
            Users = new List<UserEntity>();
            Projects = new List<ProjectEntity>();
        }
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
        public ICollection<UserEntity> Users { get; set; }
        public ICollection<ProjectEntity> Projects { get; set; }
    }
}
