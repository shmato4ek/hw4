﻿using HW4.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HW4.DAL.Entities
{
    [Table("Projects")]
    public class ProjectEntity : BaseEntity
    {
        public ProjectEntity()
        {
            Tasks = new List<TaskEntity>();
        }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime? CreatedAt { get; set; }
        public UserEntity Author { get; set; }
        public TeamEntity Team { get; set; }
        public ICollection<TaskEntity> Tasks{get; private set;}
    }
}
