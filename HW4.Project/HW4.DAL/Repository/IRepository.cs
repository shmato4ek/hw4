﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace HW4.DAL.Repository
{
    public interface IRepository<T>
        where T : class
    {
        Task<T> Create(T item);
        Task<List<T>> GetAll();
        Task<T> Get(int id);
        Task<T> Update(T item);
        Task Delete(int id);
    }
}
