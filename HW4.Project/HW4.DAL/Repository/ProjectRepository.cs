﻿using HW4.DAL.EF;
using HW4.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW4.DAL.Repository
{
    public class ProjectRepository : IRepository<ProjectEntity>
    {
        private ProjectsContext _context;
        public ProjectRepository(ProjectsContext context)
        {
            _context = context;
        }
        public async Task<ProjectEntity> Create(ProjectEntity item)
        {
            await _context.Projects.AddAsync(item);
            return item;
        }

        public async Task Delete(int id)
        {
            var projects = await _context.Projects.ToListAsync();
            var project = projects.Where(project => project.Id == id).FirstOrDefault();
            _context.Projects.Remove(project);
        }

        public async Task<ProjectEntity> Get(int id)
        {
            var projects = await _context.Projects.ToListAsync();
            var result = projects.Where(project => project.Id == id).FirstOrDefault();
            return result;
        }

        public Task<List<ProjectEntity>> GetAll()
        {
            return _context.Projects.ToListAsync();
        }

        public async Task<ProjectEntity> Update(ProjectEntity item)
        {
            var entity = await _context.Projects.FirstOrDefaultAsync(p => p.Id == item.Id);
            if (entity == null)
            {
                throw new Exception();
            }
            entity.Name = item.Name;
            entity.Description = item.Description;
            _context.Projects.Update(entity);
            return item;
        }
    }
}
