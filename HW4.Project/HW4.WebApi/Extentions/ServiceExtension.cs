﻿using HW4.BLL.Services;
using Microsoft.Extensions.DependencyInjection;
using HW4.BLL.MappingProfiles;
using System.Reflection;
using HW4.DAL.UOW;
using HW4.BLL.Interfaces;
using Common.DTO;
using Common.DTO.Project;
using Common.DTO.Task;
using Common.DTO.Team;
using Common.DTO.User;

namespace HW4.WebAPI.Extentions
{
    public static class ServiceExtension
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddScoped<DataService>();
            services.AddScoped<LinqService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IService<ProjectDTO, ProjectCreatedDTO>, ProjectService>();
            services.AddScoped<IService<TaskDTO, TaskCreatedDTO>, TaskService>();
            services.AddScoped<IService<TeamDTO, TeamCreatedDTO>, TeamService>();
            services.AddScoped<IService<UserDTO, UserCreatedDTO>, UserService>();
        }   

        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TaskForLinqProfile>();
                cfg.AddProfile<ProjectLinqProfile>();
                cfg.AddProfile<UserStructProfile>();
                cfg.AddProfile<UserForLinqProfile>();
                cfg.AddProfile<UserCreatedProfile>();
                cfg.AddProfile<TaskCreatedProfile>();
                cfg.AddProfile<ProjectCreatedProfile>();
                cfg.AddProfile<TeamCreatedProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
