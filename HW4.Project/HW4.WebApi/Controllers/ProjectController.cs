﻿using Common.DTO;
using Common.DTO.Project;
using HW4.BLL.Interfaces;
using HW4.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HW4.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IService<ProjectDTO, ProjectCreatedDTO> _projectService;

        public ProjectController(IService<ProjectDTO, ProjectCreatedDTO> projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<ActionResult<List<ProjectDTO>>> Get()
        {
            var projects = await _projectService.GetAll();
            return Ok(projects);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetById(int id)
        {
            try
            {
                var result = await _projectService.Get(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"Project with id {id} was not found");
            }
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> CreateProject([FromBody] ProjectCreatedDTO project)
        {
            try
            {
                var newProject = await _projectService.Add(project);
                return Created($"~api/project/{newProject.Id}", newProject);
            }
            catch(Exception)
            {
                throw new Exception("This structure of project is uncorrect");
            }
        }

        [HttpPut]
        public async Task<ActionResult<ProjectDTO>> UpdateProject([FromBody] ProjectDTO project)
        {
            try
            {
                var updatedProject = await _projectService.Update(project);
                return Created($"~api/project/{updatedProject.Id}", updatedProject);
            }
            catch(Exception)
            {
                throw new Exception("This structure of project is uncorrect");
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProject(int id)
        {
            try
            {
                await _projectService.Delete(id);
                return NoContent();
            }
            catch(Exception)
            {
                throw new Exception($"Project with id {id} was not found");
            }
        }
    }
}
