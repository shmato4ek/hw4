﻿using Common.DTO;
using Common.DTO.Team;
using HW4.BLL.Interfaces;
using HW4.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HW4.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly IService<TeamDTO, TeamCreatedDTO> _teamService;

        public TeamController(IService<TeamDTO, TeamCreatedDTO> teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<List<TeamDTO>>> Get()
        {
            var result = await _teamService.GetAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetById(int id)
        {
            try
            {
                var result = await _teamService.Get(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"Team with id {id} was not found");
            }
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> CreateTeam([FromBody] TeamCreatedDTO team)
        {
            try
            {
                var newTeam = await _teamService.Add(team);
                return Created($"~api/team/{newTeam.Id}", newTeam);
            }
            catch(Exception)
            {
                throw new Exception("This structure of team is uncorrect");
            }
        }

        [HttpPut]
        public async Task<ActionResult<TeamDTO>> UpdateTeam([FromBody] TeamDTO team)
        {
            try
            {
                var updatedTeam = await _teamService.Update(team);
                return Created($"~api/team/{updatedTeam.Id}", updatedTeam);
            }
            catch(Exception)
            {
                throw new Exception("This structure of team is uncorrect");
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteTeam(int id)
        {
            try
            {
                await _teamService.Delete(id);
                return NoContent();
            }
            catch(Exception)
            {
                throw new Exception($"Task with id {id} was not found");
            }
        }
    }
}
