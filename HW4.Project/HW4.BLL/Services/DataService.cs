﻿using HW4.BLL.ModelsForLINQ;
using HW4.DAL.Entities;
using HW4.DAL.UOW;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW4.BLL.Services
{
    public class DataService
    {
        private readonly List<Project> projects;
        private IUnitOfWork Data { get; set; }
        public DataService(IUnitOfWork uow)
        {
            projects = new List<Project>();
            Data = uow;
        }

        public async Task<List<Project>> GetRequest()
        {
            List<ProjectEntity> allProjects = await Data.Projects.GetAll();
            List<TaskEntity> allTasks = await Data.Tasks.GetAll();
            List<TeamEntity> allTeams = await Data.Teams.GetAll();
            List<UserEntity> allUsers = await Data.Users.GetAll();
            var foundProjects = from project in allProjects
                                join team in allTeams on project.TeamId equals team.Id
                                join author in allUsers on project.AuthorId equals author.Id
                                join task in allTasks on project.Id equals task.ProjectId into tasks
                                select new Project
                                {
                                    Id = project.Id,
                                    Name = project.Name,
                                    Description = project.Description,
                                    CreatedAt = project.CreatedAt,
                                    DeadLine = project.Deadline,
                                    Team = new Team
                                    {
                                        Id = team.Id,
                                        Name = team.Name,
                                        CreatedAt = team.CreatedAt,
                                        Users = (
                                            from user in allUsers
                                            where user.TeamId == team.Id
                                            select new User
                                            {
                                                Id = user.Id,
                                                FirstName = user.FirstName,
                                                Birthday = user.Birthday,
                                                Email = user.Email,
                                                LastName = user.LastName,
                                                RegisteredAt = user.RegisteredAt
                                            }
                                        ).ToList()
                                    },

                                    Author = new User { Id = author.Id, FirstName = author.FirstName, LastName = author.LastName, RegisteredAt = author.RegisteredAt, Email = author.Email, Birthday = author.Birthday },
                                    Tasks = (from taskEl in tasks
                                             join performer in allUsers on taskEl.PerformerId equals performer.Id
                                             select new ProjectTask
                                             {
                                                 Id = taskEl.Id,
                                                 Name = taskEl.Name,
                                                 Description = taskEl.Description,
                                                 CreatedAt = taskEl.CreatedAt,
                                                 FinishedAt = taskEl.FinishedAtDate,
                                                 State = taskEl.State,
                                                 Performer = new User
                                                 {
                                                     Id = performer.Id,
                                                     FirstName = performer.FirstName,
                                                     Birthday = performer.Birthday,
                                                     RegisteredAt = performer.RegisteredAt,
                                                     Email = performer.Email,
                                                     LastName = performer.LastName
                                                 }
                                             }).ToList()
                                };

            foreach (var item in foundProjects)
            {
                projects.Add(item);
            }

            return projects;
        }
    }
}
