﻿using AutoMapper;
using Common.DTO;
using HW4.BLL.Interfaces;
using HW4.BLL.Services.Abstract;
using HW4.DAL.UOW;
using System.Collections.Generic;
using HW4.DAL.Entities;
using System.Threading.Tasks;
using Common.DTO.Project;

namespace HW4.BLL.Services
{
    public class ProjectService : BaseService, IService<ProjectDTO, ProjectCreatedDTO>
    {
        public ProjectService(IMapper mapper, IUnitOfWork uow) : base(mapper, uow) { }

        public async Task<List<ProjectDTO>> GetAll()
        {
            var result = await data.Projects.GetAll();

            return _mapper.Map<List<ProjectDTO>>(result);
        }

        public async Task<ProjectDTO> Get(int id)
        {
            var project = await data.Projects.Get(id);

            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task<ProjectDTO> Add(ProjectCreatedDTO project)
        {
            var newProject = _mapper.Map<ProjectEntity>(project);
            await data.Projects.Create(newProject);
            await data.Save();

            return _mapper.Map<ProjectDTO>(data.Projects.Get(newProject.Id));
        }

        public async Task<ProjectDTO> Update(ProjectDTO project)
        {
            var newProject = _mapper.Map<ProjectEntity>(project);
            await data.Projects.Update(newProject);
            await data.Save();

            return _mapper.Map<ProjectDTO>(data.Projects.Get(newProject.Id));
        }

        public async Task Delete(int id)
        {
            await data.Projects.Delete(id);
            await data.Save();
        }
    }
}
