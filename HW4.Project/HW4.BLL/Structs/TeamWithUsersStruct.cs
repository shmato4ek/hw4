﻿using HW4.BLL.ModelsForLINQ;
using System.Collections.Generic;

namespace HW4.BLL.Structs
{
    public struct TeamWithUsersStruct
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
