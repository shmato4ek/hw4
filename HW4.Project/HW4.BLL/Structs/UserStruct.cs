﻿using Common.DTO;

namespace HW4.BLL.Structs
{
    public struct UserStruct
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int AmmountOfTaskInLastProject { get; set; }
        public int AmmoutOfNotFinishedOrClosedTasks { get; set; }
        public TaskDTO TheLongestTask { get; set; }
    }
}
