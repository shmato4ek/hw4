﻿using Common.DTO;

namespace HW4.BLL.Structs
{
    public struct ProjectStruct
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO theLongestTask { get; set; }
        public TaskDTO theShortestTask { get; set; }
        public int? ammountOfUsers { get; set; }
    }
}
