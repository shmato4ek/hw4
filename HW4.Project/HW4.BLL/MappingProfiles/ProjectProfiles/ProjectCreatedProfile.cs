﻿using AutoMapper;
using Common.DTO.Project;
using HW4.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW4.BLL.MappingProfiles
{
    public class ProjectCreatedProfile : Profile
    {
        public ProjectCreatedProfile()
        {
            CreateMap<ProjectCreatedDTO, ProjectEntity>();
        }
    }
}
