﻿using AutoMapper;
using Common.DTO;
using HW4.DAL.Entities;

namespace HW4.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectEntity, ProjectDTO>();
            CreateMap<ProjectDTO, ProjectEntity>();
        }
    }
}
