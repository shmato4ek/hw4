﻿using AutoMapper;
using Common.DTO;
using HW4.BLL.ModelsForLINQ;

namespace HW4.BLL.MappingProfiles
{
    public class ProjectLinqProfile : Profile
    {
        public ProjectLinqProfile()
        {
            CreateMap<Project, ProjectDTO>();
        }
    }
}
