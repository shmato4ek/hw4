﻿using AutoMapper;
using Common.DTO;
using Common.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW4.BLL.MappingProfiles
{
    public class TeamCreatedProfile : Profile
    {
        public TeamCreatedProfile()
        {
            CreateMap<TaskCreatedDTO, TaskDTO>();
        }
    }
}
