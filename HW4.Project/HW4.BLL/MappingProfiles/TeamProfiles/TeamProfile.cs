﻿using AutoMapper;
using Common.DTO;
using HW4.DAL.Entities;

namespace HW4.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamEntity, TeamDTO>();
            CreateMap<TeamDTO, TeamEntity>();
        }
    }
}
