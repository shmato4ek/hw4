﻿using AutoMapper;
using Common.DTO;
using HW4.DAL.Entities;

namespace HW4.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskEntity, TaskDTO>();
            CreateMap<TaskDTO, TaskEntity>();
        }
    }
}
