﻿using AutoMapper;
using Common.DTO;
using HW4.BLL.ModelsForLINQ;

namespace HW4.BLL.MappingProfiles
{
    public class TaskForLinqProfile : Profile
    {
        public TaskForLinqProfile()
        {
            CreateMap<ProjectTask, TaskDTO>().ForMember(task => task.PerformerId, src => src.MapFrom(member => member.Performer.Id));
            CreateMap<TaskDTO, ProjectTask>();
        }
    }
}
