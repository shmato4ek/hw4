﻿using AutoMapper;
using Common.DTO;
using HW4.BLL.Structs;

namespace HW4.BLL.MappingProfiles
{
    public class UserStructProfile : Profile
    {
        public UserStructProfile()
        {
            CreateMap<UserStruct, UserStructDTO>();
        }
    }
}
