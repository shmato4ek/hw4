﻿using AutoMapper;
using Common.DTO;
using HW4.DAL.Entities;

namespace HW4.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserEntity, UserDTO>();
            CreateMap<UserDTO, UserEntity>();
        }
    }
}
