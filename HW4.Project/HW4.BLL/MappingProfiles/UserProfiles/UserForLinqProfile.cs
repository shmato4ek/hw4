﻿using AutoMapper;
using Common.DTO;
using HW4.BLL.ModelsForLINQ;

namespace HW4.BLL.MappingProfiles
{
    public class UserForLinqProfile : Profile
    {
        public UserForLinqProfile()
        {
            CreateMap<User, UserDTO>();
        }
    }
}
