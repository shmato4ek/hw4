﻿using HW4.DAL.Entities;
using System;

namespace HW4.BLL.ModelsForLINQ
{
    public class ProjectTask
    {
        public User Performer { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskStates State { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
