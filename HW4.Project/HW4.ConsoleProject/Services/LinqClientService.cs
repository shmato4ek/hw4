﻿using Common.DTO;
using HW4.BLL.Structs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HW4.ConsoleProject.Services
{
    class LinqClientService
    {
        private readonly HttpClientService _http;
        private readonly string _URL = "linq";
        public LinqClientService()
        {
            _http = new HttpClientService();
        }
        public async Task<List<ProjectDictionaryDTO>> GetAmountOfTasksOfProject(int userId)
        {
            return await _http.Get<List<ProjectDictionaryDTO>>($"{_URL}/user/{userId}/tasks/amount");
        }

        public async Task<List<TaskDTO>> GetConcreteUsersTask(int userId)
        {
            return await _http.Get<List<TaskDTO>>($"{_URL}/user/{userId}/tasks");
        }

        public async Task<List<TaskDTO>> GetTasksCompletedIn2021(int userId)
        {
            return await _http.Get<List<TaskDTO>>($"{_URL}/user/{userId}/tasks/finished");
        }

        public async Task<List<TeamWithUsersStruct>> GetTeamsWith10MoreUser()
        {
            return await _http.Get<List<TeamWithUsersStruct>>($"{_URL}/teams/older-then10");
        }

        public async Task<List<UserDictionaryDTO>> GetSortedListOfUsers()
        {
            return await _http.Get<List<UserDictionaryDTO>>($"{_URL}/users/sorted");
        }

        public async Task<UserStructDTO> GetUsersInfo(int userId)
        {
            return await _http.Get<UserStructDTO>($"{_URL}/user/{userId}/info");
        }

        public async Task<List<ProjectStruct>> GetProjectsInfo()
        {
            return await _http.Get<List<ProjectStruct>>($"{_URL}/projects/info");
        }
    }
}
