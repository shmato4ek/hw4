﻿using Common.DTO;
using Common.DTO.Task;
using HW4.ConsoleProject.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HW4.ConsoleTask.Services
{
    class TaskClientService
    {
        private readonly HttpClientService _http;
        private readonly string _URL = "task";
        public TaskClientService()
        {
            _http = new HttpClientService();
        }
        public async Task<List<TaskDTO>> GetAll()
        {
            return await _http.GetAll<TaskDTO>(_URL);
        }
        public async Task<TaskDTO> GetById(int id)
        {
            return await _http.GetById<TaskDTO>(_URL, id);
        }
        public async Task<TaskDTO> Create(TaskCreatedDTO taskCreatedDTO)
        {
            return await _http.Create<TaskDTO, TaskCreatedDTO>(_URL, taskCreatedDTO);
        }
        public async Task<TaskDTO> Update(TaskDTO taskDTO)
        {
            return await _http.Update<TaskDTO>(_URL, taskDTO);
        }
        public async Task Delete(int id)
        {
            await _http.Delete(_URL, id);
        }
    }
}
