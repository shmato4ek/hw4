﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Common.DTO;
using Common.DTO.Project;

namespace HW4.ConsoleProject.Services
{
    class ProjectClientService
    {
        private readonly HttpClientService _http;
        private readonly string _URL = "project";
        public ProjectClientService()
        {
            _http = new HttpClientService();
        }
        public async Task<List<ProjectDTO>> GetAll()
        {
            return await _http.GetAll<ProjectDTO>(_URL);
        }
        public async Task<ProjectDTO> GetById(int id)
        {
            return await _http.GetById<ProjectDTO>(_URL, id);
        }
        public async Task<ProjectDTO> Create(ProjectCreatedDTO projectCreatedDTO)
        {
            return await _http.Create<ProjectDTO, ProjectCreatedDTO>(_URL, projectCreatedDTO);
        }
        public async Task<ProjectDTO> Update(ProjectDTO projectDTO)
        {
            return await _http.Update<ProjectDTO>(_URL, projectDTO);
        }
        public async Task Delete(int id)
        {
            await _http.Delete(_URL, id);
        }
    }
}
