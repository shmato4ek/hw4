﻿using Common.DTO;
using Common.DTO.Team;
using HW4.ConsoleProject.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HW4.ConsoleTeam.Services
{
    class TeamClientService
    {
        private readonly HttpClientService _http;
        private readonly string _URL = "team";
        public TeamClientService()
        {
            _http = new HttpClientService();
        }
        public async Task<List<TeamDTO>> GetAll()
        {
            return await _http.GetAll<TeamDTO>(_URL);
        }
        public async Task<TeamDTO> GetById(int id)
        {
            return await _http.GetById<TeamDTO>(_URL, id);
        }
        public async Task<TeamDTO> Create(TeamCreatedDTO teamCreatedDTO)
        {
            return await _http.Create<TeamDTO, TeamCreatedDTO>(_URL, teamCreatedDTO);
        }
        public async Task<TeamDTO> Update(TeamDTO teamDTO)
        {
            return await _http.Update<TeamDTO>(_URL, teamDTO);
        }
        public async Task Delete(int id)
        {
            await _http.Delete(_URL, id);
        }
    }
}
