﻿using Common.DTO;
using HW4.ConsoleTask.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW4.ConsoleProject.Services
{
    public class TimerService
    {
        public Task<int> MarkRandomTaskWithDelay(double delay)
        {
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            TaskClientService _taskService = new TaskClientService();
            var aTimer = new System.Timers.Timer();
            aTimer.Interval = delay;
            aTimer.AutoReset = false;
            aTimer.Elapsed += async(o, e) =>
            {
                var rnd = new Random();
                var allTasks = await _taskService.GetAll();
                var notFinishedTasks = allTasks.Where(task => task.State != TaskStatesDTO.Finished);
                var newTask = notFinishedTasks.OrderBy(x => rnd.Next()).FirstOrDefault();
                newTask.State = TaskStatesDTO.Finished;
                if (newTask != null)
                {
                    var updatedTask = await _taskService.Update(newTask);
                    tcs.SetResult(newTask.Id);
                    Console.WriteLine($"Task with id {updatedTask.Id} was marked as completed!");
                }
                else
                {
                    throw new Exception("There is no not complited tasks!");
                }

            };
            aTimer.Start();
            var tcsTask = tcs.Task;
            return tcsTask;
        }
    }
}
    