﻿using Common.DTO;
using Common.DTO.User;
using HW4.ConsoleProject.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HW4.ConsoleUser.Services
{
    class UserClientService
    {
        private readonly HttpClientService _http;
        private readonly string _URL = "user";
        public UserClientService()
        {
            _http = new HttpClientService();
        }
        public async Task<List<UserDTO>> GetAll()
        {
            return await _http.GetAll<UserDTO>(_URL);
        }
        public async Task<UserDTO> GetById(int id)
        {
            return await _http.GetById<UserDTO>(_URL, id);
        }
        public async Task<UserDTO> Create(UserCreatedDTO userCreatedDTO)
        {
            return await _http.Create<UserDTO, UserCreatedDTO>(_URL, userCreatedDTO);
        }
        public async Task<UserDTO> Update(UserDTO userDTO)
        {
            return await _http.Update<UserDTO>(_URL, userDTO);
        }
        public async Task Delete(int id)
        {
            await _http.Delete(_URL, id);
        }
    }
}
