﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HW4.ConsoleProject.Services
{
    class HttpClientService
    {
        private const string BASE_URL = "https://localhost:44320/api";
        private readonly HttpClient _client;
        public HttpClientService()
        {
            _client = new HttpClient();
        }
        public async Task<T> Get<T>(string url)
        {
            try
            {
                var response = await _client.GetStringAsync($"{BASE_URL}/{url}");
                var result = JsonConvert.DeserializeObject<T>(response);
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(T);
            }
        }
        public async Task<List<T>> GetAll<T>(string url)
        {
            try
            {
                var response = await _client.GetStringAsync($"{BASE_URL}/{url}");
                var result = JsonConvert.DeserializeObject<List<T>>(response);
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
        public async Task<T> GetById<T>(string url, int id)
        {
            try
            {
                var response = await _client.GetStringAsync($"{BASE_URL}/{url}/{id}");
                var result = JsonConvert.DeserializeObject<T>(response);
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(T);
            }
        }
        public async Task<T> Create<T, K>(string url, K createdDto)
        {
            try
            {
                var createdJson = JsonConvert.SerializeObject(createdDto);
                var stringContent = new StringContent(createdJson, Encoding.UTF8, "application/json");
                var response = await _client.PostAsync($"{BASE_URL}/{url}", stringContent);
                string responseJson = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<T>(responseJson);
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(T);
            }
        }
        public async Task<T> Update<T>(string url, T dto)
        {
            try
            {
                var updatedJson = JsonConvert.SerializeObject(dto);
                var stringContent = new StringContent(updatedJson, Encoding.UTF8, "application/json");
                var response = await _client.PutAsync($"{BASE_URL}/{url}", stringContent);
                string responseJson = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<T>(responseJson);
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(T);
            }
        }
        public async Task Delete(string url, int id)
        {
            var response = await _client.DeleteAsync($"{BASE_URL}/{url}/{id}");
        }
    }
}
