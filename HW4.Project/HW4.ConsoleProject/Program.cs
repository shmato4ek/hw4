﻿using HW4.ConsoleProject.ConsoleMenu;
using HW4.ConsoleTask.ConsoleMenu;
using HW4.ConsoleTeam.ConsoleMenu;
using HW4.ConsoleUser.ConsoleMenu;
using System;
using System.Threading.Tasks;

namespace HW4.ConsoleProject
{
    class Program
    {
        static async Task Main(string[] args)
        {
            bool flag = true;
            var projectMenu = new ProjectMenu();
            var taskMenu = new TaskMenu();
            var userMenu = new UserMenu();
            var teamMenu = new TeamMenu();
            var linqMenu = new LinqMenu();
            var timerMenu = new TimerMenu();

            while (flag)
            {
                Console.WriteLine("*********************************************************************************************************");
                Console.WriteLine("\tWelcome to my console :)\n\tThere is list of all tasks:\n\n");
                Console.WriteLine("1. Tasks.\n" +
                    "2. Projects.\n" +
                    "3. Teams.\n" +
                    "4. Users.\n" +
                    "5. LINQ\n" +
                    "6. Mark random task with delay\n" +
                    "0. Exit\n");
                Console.Write("Write the number of task: ");
                int answer1 = Int32.Parse(Console.ReadLine());
                switch (answer1)
                {
                    case 1:
                        taskMenu.Display();
                        await taskMenu.ExecuteTask();
                        break;
                    case 2:
                        projectMenu.Display();
                        await projectMenu.ExecuteTask();
                        break;
                    case 3:
                        teamMenu.Display();
                        await teamMenu.ExecuteTask();
                        break;
                    case 4:
                        userMenu.Display();
                        await userMenu.ExecuteTask();
                        break;
                    case 5:
                        linqMenu.Display();
                        await linqMenu.ExecuteTask();
                        break;
                    case 6:
                        await timerMenu.ExecuteTask();
                        break;
                    case 0:
                        flag = false;
                        break;
                    default:
                        break;
                }
            }
        }

       
    }
}
