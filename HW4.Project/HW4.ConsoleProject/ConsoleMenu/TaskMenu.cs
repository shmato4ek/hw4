﻿using Common.DTO;
using Common.DTO.Task;
using HW4.ConsoleTask.Services;
using System;
using System.Threading.Tasks;

namespace HW4.ConsoleTask.ConsoleMenu
{
    class TaskMenu
    {
        private readonly TaskClientService _taskService;
        public TaskMenu()
        {
            _taskService = new TaskClientService();
        }
        public void Display()
        {
            Console.WriteLine("\n\n\tTasks:\n");
            Console.WriteLine("1. Get all.\n" +
                "2. Get task by id.\n" +
                "3. Update task.\n" +
                "4. Delete task.\n" +
                "5. Add task.\n");
        }

        public async Task ExecuteTask()
        {
            Console.Write(" Write number: ");
            int answer = Int32.Parse(Console.ReadLine());
            switch (answer)
            {
                case 1:
                    await GetAllTasks();
                    break;
                case 2:
                    await GetTaskById();
                    break;
                case 3:
                    await UpdateTask();
                    break;
                case 4:
                    await DeleteTask();
                    break;
                case 5:
                    await AddNewTask();
                    break;
                default:
                    break;
            }
        }

        private async Task GetAllTasks()
        {
            Console.WriteLine("\n\nAll tasks:\n\n");
            var allTasks = await _taskService.GetAll();
            foreach (var item in allTasks)
            {
                Console.WriteLine($"Id: {item.Id}, name: {item.Name}, state: {item.State}");
            }
        }

        private async Task GetTaskById()
        {
            try
            {
                Console.Write("\n\nWrite id: ");
                int taskId = Int32.Parse(Console.ReadLine());
                var task = await _taskService.GetById(taskId);
                Console.WriteLine($"Your task is {task.Id}. {task.Name}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task UpdateTask()
        {
            try
            {
                Console.Write("\n\nWrite task id: ");
                int taskId = Int32.Parse(Console.ReadLine());
                Console.Write("\nWrite name: ");
                string taskName = Console.ReadLine();
                Console.Write("\nWrite description: ");
                string taskDescription = Console.ReadLine();
                Console.Write("\nWrite user id: ");
                var userId = Int32.Parse(Console.ReadLine());
                Console.Write("\nWrite project id: ");
                var projectId = Int32.Parse(Console.ReadLine());
                var taskDto = new TaskDTO { Id = taskId, Name = taskName, Description = taskDescription, PerformerId = userId, ProjectId = projectId, State = TaskStatesDTO.Started };
                var task = await _taskService.Update(taskDto);
                Console.WriteLine($"Task was sucessfully updated! Task id: {task.Id}, name: {task.Name}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task DeleteTask()
        {
            try
            {
                Console.Write("\n\nWrite id: ");
                int taskId = Int32.Parse(Console.ReadLine());
                await _taskService.Delete(taskId);
                Console.WriteLine($"Task with id: {taskId} was sucessfully deleted!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task AddNewTask()
        {
            try
            {
                Console.Write("\nWrite name: ");
                var taskName = Console.ReadLine();
                Console.Write("\nWtite description: ");
                var taskDescription = Console.ReadLine();
                Console.Write("\nWrite performer id: ");
                var performerId = Int32.Parse(Console.ReadLine());
                Console.Write("\nWrite project id: ");
                var projectId = Int32.Parse(Console.ReadLine());
                var taskDto = new TaskCreatedDTO { Name = taskName, Description = taskDescription, PerformerId = performerId, ProjectId = projectId };
                var task = await _taskService.Create(taskDto);
                Console.WriteLine($"Task with id {task.Id} and name {task.Name} was sucessfully added!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
