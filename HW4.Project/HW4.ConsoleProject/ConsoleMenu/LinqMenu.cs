﻿using Common.DTO;
using HW4.ConsoleProject.Services;
using System;
using System.Threading.Tasks;

namespace HW4.ConsoleProject.ConsoleMenu
{
    class LinqMenu
    {
        private readonly LinqClientService _linqService;
        public LinqMenu()
        {
            _linqService = new LinqClientService();
        }
        public void Display ()
        {
            Console.WriteLine("\n\n\tLINQ\n\n");
            Console.WriteLine("1. Ammount of tasks of concrete user`s project.\n" +
                "2. List of concrete user`s tasks with length < 45 symbols.\n" +
                "3. List of concrete user`s tasks that were completed in 2021.\n" +
                "4. List of teams with 10 and more y.o. users, sorted by time of registration discending and grouped by teams.\n" +
                "5. List of users sorted by first name wits tasks, sorted by length of name discending.\n" +
                "6. Special struct#1.\n" +
                "7. Special struct#2.\n" +
                "0. Exit\n" +
                "*********************************************************************************************************\n\n");
        }

        public async Task ExecuteTask()
        {
            Console.Write("\tWrite the number of task: ");
            int answer = Int32.Parse(Console.ReadLine());
            switch (answer)
            {
                case 1:
                    await GetAmountOfTasksOfProject();
                    break;
                case 2:
                    await GetConcreteUsersTask();
                    break;
                case 3:
                    await GetTasksCompletedIn2021();
                    break;
                case 4:
                    await GetTeamsWith10MoreUser();
                    break;
                case 5:
                    await GetSortedListOfUsers();
                    break;
                case 6:
                    await GetUsersInfo();
                    break;
                case 7:
                    await GetProjectsInfo();
                    break;
                default:
                    break;
            }
        }

        private async Task GetAmountOfTasksOfProject()
        {
            try
            {
                Console.WriteLine("\n\n\t1. Ammount of tasks of concrete user`s project.\n\n");
                Console.Write("Write id of user: ");
                int userId = Int32.Parse(Console.ReadLine());
                Console.WriteLine();
                var tasks = await _linqService.GetAmountOfTasksOfProject(userId);
                foreach (var item in tasks)
                {
                    Console.WriteLine($"Project name: {item.Key.Name}, ammount of tasks: {item.Value}");
                }
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task GetConcreteUsersTask()
        {
            try
            {
                Console.WriteLine("\n\n\t2. List of concrete user`s tasks with length < 45 symbols.\n\n");
                Console.Write("Write id of user: ");
                int userId = Int32.Parse(Console.ReadLine());
                Console.WriteLine();
                var tasks = await _linqService.GetConcreteUsersTask(userId);
                foreach (var item in tasks)
                {
                    Console.WriteLine($"Task name: {item.Name}({item.Description}).");
                }
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task GetTasksCompletedIn2021()
        {
            try
            {
                Console.WriteLine("\n\n\t3. List of concrete user`s tasks that were completed in 2021.\n\n");
                Console.Write("Write id of user: ");
                int userId = Int32.Parse(Console.ReadLine());
                Console.WriteLine();
                var tasks = await _linqService.GetTasksCompletedIn2021(userId);
                foreach (var item in tasks)
                {
                    Console.WriteLine($"Id: {item.Id}, name: {item.Name}");
                }
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task GetTeamsWith10MoreUser()
        {
            Console.WriteLine("\n\n\t4. List of teams with 10 and more y.o. users, sorted by time of registration discending and grouped by teams.\n\n");
            var teams = await _linqService.GetTeamsWith10MoreUser();
            foreach (var item in teams)
            {
                Console.WriteLine($"\tTeam id: {item.Id}, team name: {item.TeamName}, users: ");
                foreach (var user in item.Users)
                {
                    Console.Write("{0} {1},", user.LastName, user.FirstName);
                }
                Console.WriteLine("\n");
            }
            Console.WriteLine();

        }

        private async Task GetSortedListOfUsers()
        {
            Console.WriteLine("\n\n\t5. List of users sorted by first name wits tasks, sorted by length of name discending.\n\n");
            var users = await _linqService.GetSortedListOfUsers();
            foreach (var item in users)
            {
                Console.WriteLine($"\tUser name: { item.Key.FirstName} {item.Key.LastName}, tasks:");
                foreach (var t in item.Value)
                {
                    Console.Write("{0}, ", t.Name);
                }
                Console.WriteLine("\n");
            }
            Console.WriteLine();

        }

        private async Task GetUsersInfo()
        {
            try
            {
                Console.WriteLine("\n\n\t6. Special struct#1.\n\n");
                Console.Write("Write id of user: ");
                int userId = Int32.Parse(Console.ReadLine());
                Console.WriteLine();
                var user = await _linqService.GetUsersInfo(userId);
                Console.WriteLine($"UserId: {user.User.Id}, user name: {user.User.FirstName} {user.User.LastName}.");
                Console.WriteLine($"Last user`s project: {user.LastProject.Name}({user.LastProject.Description}) created at: {user.LastProject.CreatedAt}");
                Console.WriteLine($"Ammount of tasks of last user`s project: {user.AmmountOfTaskInLastProject}.");
                Console.WriteLine($"Ammount of not finished or canceled tasks: {user.AmmoutOfNotFinishedOrClosedTasks}.");
                Console.WriteLine($"The longest user`s task: {user.TheLongestTask.Name}({user.TheLongestTask.Description})");
                Console.WriteLine();
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task GetProjectsInfo()
        {
            Console.WriteLine("\n\n\t7. Special struct#2.\n\n");
            var projects = await _linqService.GetProjectsInfo();
            foreach (var project in projects)
            {
                Console.WriteLine($"\tProject: {project.Project.Id}. {project.Project.Name}({project.Project.Description}).");
                if (project.theLongestTask != null)
                {
                    Console.WriteLine($"The longest task: {project.theLongestTask.Name}({project.theLongestTask.Description}).");
                    Console.WriteLine($"The shortest task: {project.theShortestTask.Name}({project.theShortestTask.Description}).");
                }

                else
                {
                    Console.WriteLine("This project doesn`t contain any task");
                }

                if (project.ammountOfUsers != 0)
                {
                    Console.WriteLine($"The ammount of users in project`s team: {project.ammountOfUsers}");
                }

                else
                {
                    Console.WriteLine("Description of the project <= 20 symbols and ammount of project`s task >= 3.");
                }
                Console.WriteLine("\n");
            }

        }
    }
}
