﻿using Common.DTO;
using Common.DTO.Project;
using HW4.ConsoleProject.Services;
using System;
using System.Threading.Tasks;

namespace HW4.ConsoleProject.ConsoleMenu
{
    class ProjectMenu
    {
        private readonly ProjectClientService _projectService;
        public ProjectMenu()
        {
            _projectService = new ProjectClientService();
        }
        public void Display ()
        {
            Console.WriteLine("\n\n\tProjects:\n");
            Console.WriteLine("1. Get all.\n" +
                "2. Get project by id.\n" +
                "3. Update project.\n" +
                "4. Delete project.\n" +
                "5. Add project.\n");
        }

        public async Task ExecuteTask()
        {
            Console.Write(" Write number: ");
            int answer = Int32.Parse(Console.ReadLine());
            switch (answer)
            {
                case 1:
                    await GetAllProjects();
                    break;
                case 2:
                    await GetProjectById();
                    break;
                case 3:
                    await UpdateProject();
                    break;
                case 4:
                    await DeleteProject();
                    break;
                case 5:
                    await AddNewProject();
                    break;
                default:
                    break;
            }
        }

        private async Task GetAllProjects()
        {
            Console.WriteLine("\n\nAll projects:\n\n");
            var allProjects = await _projectService.GetAll();
            foreach (var item in allProjects)
            {
                Console.WriteLine($"Id: {item.Id}, name: {item.Name}");
            }
        }

        private async Task GetProjectById()
        {
            try
            {
                Console.Write("\n\nWrite id: ");
                int projectId = Int32.Parse(Console.ReadLine());
                var project = await _projectService.GetById(projectId);
                Console.WriteLine($"Your project is {project.Id}. {project.Name}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task UpdateProject()
        {
            try
            {
                Console.Write("\n\nWrite project id: ");
                int projectId = Int32.Parse(Console.ReadLine());
                Console.Write("\nWrite name: ");
                string projectName = Console.ReadLine();
                Console.Write("\nWrite description: ");
                string projectDescription = Console.ReadLine();
                Console.Write("\nWrite author id: ");
                var userId = Int32.Parse(Console.ReadLine());
                Console.Write("\nWrite team id: ");
                var teamId = Int32.Parse(Console.ReadLine());
                var projectDto = new ProjectDTO { Id = projectId, Name = projectName, Description = projectDescription, AuthorId = userId, TeamId = teamId };
                var project = await _projectService.Update(projectDto);
                Console.WriteLine($"Project was sucessfully updated! Project id: {project.Id}. {project.Name}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task DeleteProject()
        {
            try
            {
                Console.Write("\n\nWrite id: ");
                int projectId = Int32.Parse(Console.ReadLine());
                await _projectService.Delete(projectId);
                Console.WriteLine($"Project with id: {projectId} was sucessfully deleted!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task AddNewProject()
        {
            try
            {
                Console.Write("\nWrite name: ");
                var projectName = Console.ReadLine();
                Console.Write("\nWtite description: ");
                var projectDescription = Console.ReadLine();
                Console.Write("\nWrite author id: ");
                var authorId = Int32.Parse(Console.ReadLine());
                Console.Write("\nWrite deadline. Year: ");
                var year = Int32.Parse(Console.ReadLine());
                Console.Write(", month: ");
                var month = Int32.Parse(Console.ReadLine());
                Console.Write(", day: ");
                var day = Int32.Parse(Console.ReadLine());
                var deadLine = new DateTime(year, month, day);
                Console.Write("\nWrite team id: ");
                var teamId = Int32.Parse(Console.ReadLine());
                var projectDto = new ProjectCreatedDTO { Name = projectName, Description = projectDescription, AuthorId = authorId, Deadline = deadLine, TeamId = teamId };
                var project = await _projectService.Create(projectDto);
                Console.WriteLine($"Project with id {project.Id} and name {project.Name} was sucessfully added!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
