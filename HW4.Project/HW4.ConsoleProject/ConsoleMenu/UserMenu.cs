﻿using Common.DTO;
using Common.DTO.User;
using HW4.ConsoleUser.Services;
using System;
using System.Threading.Tasks;

namespace HW4.ConsoleUser.ConsoleMenu
{
    class UserMenu
    {
        private readonly UserClientService _userService;
        public UserMenu()
        {
            _userService = new UserClientService();
        }
        public void Display ()
        {
            Console.WriteLine("\n\n\tUsers:\n");
            Console.WriteLine("1. Get all.\n" +
                "2. Get user by id.\n" +
                "3. Update user.\n" +
                "4. Delete user.\n" +
                "5. Add user.\n");
        }

        public async Task ExecuteTask()
        {
            Console.Write(" Write number: ");
            int answer = Int32.Parse(Console.ReadLine());
            switch (answer)
            {
                case 1:
                    await GetAllUsers();
                    break;
                case 2:
                    await GetUserById();
                    break;
                case 3:
                    await UpdateUser();
                    break;
                case 4:
                    await DeleteUser();
                    break;
                case 5:
                    await AddNewUser();
                    break;
                default:
                    break;
            }
        }

        private async Task GetAllUsers()
        {
            Console.WriteLine("\n\nAll users:\n\n");
            var allUsers = await _userService.GetAll();
            foreach (var item in allUsers)
            {
                Console.WriteLine($"Id: {item.Id}, name: {item.FirstName} {item.LastName}");
            }
        }

        private async Task GetUserById()
        {
            try
            {
                Console.Write("\n\nWrite id: ");
                int userId = Int32.Parse(Console.ReadLine());
                var user = await _userService.GetById(userId);
                Console.WriteLine($"Your user is {user.Id}. {user.FirstName} {user.LastName}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task UpdateUser()
        {
            try
            {
                Console.Write("\n\nWrite user id: ");
                int userId = Int32.Parse(Console.ReadLine());
                Console.Write("\nWrite first name: ");
                string userFirstName = Console.ReadLine();
                Console.Write("\nWrite last name: ");
                string userLastName = Console.ReadLine();
                var userDto = new UserDTO { Id = userId, FirstName = userFirstName, LastName = userLastName };
                var user = await _userService.Update(userDto);
                Console.WriteLine($"User was sucessfully updated! User id: {user.Id}. {user.FirstName} {user.LastName}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task DeleteUser()
        {
            try
            {
                Console.Write("\n\nWrite id: ");
                int userId = Int32.Parse(Console.ReadLine());
                await _userService.Delete(userId);
                Console.WriteLine("User with id: {userId} was sucessfully deleted!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task AddNewUser()
        {
            try
            {
                Console.Write("\nWrite first name: ");
                var userFirstName = Console.ReadLine();
                Console.Write("\nWrite last name: ");
                var userLastName = Console.ReadLine();
                Console.Write("\nWrite your birthday. Year: ");
                var year = Int32.Parse(Console.ReadLine());
                Console.Write(", month: ");
                var month = Int32.Parse(Console.ReadLine());
                Console.Write(", day: ");
                var day = Int32.Parse(Console.ReadLine());
                var birthday = new DateTime(year, month, day);
                Console.Write("\nWrite email: ");
                var email = Console.ReadLine();
                Console.Write("\nWrite team id: ");
                var teamId = Int32.Parse(Console.ReadLine());
                var userDto = new UserCreatedDTO { FirstName = userFirstName, LastName = userLastName, Birthday = birthday, Email = email, TeamId = teamId };
                var user = await _userService.Create(userDto);
                Console.WriteLine($"User with id {user.Id} and name {user.FirstName} {user.Id} was sucessfully posted!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
