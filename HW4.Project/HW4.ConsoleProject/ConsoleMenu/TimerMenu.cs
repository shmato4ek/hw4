﻿using HW4.ConsoleProject.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW4.ConsoleProject.ConsoleMenu
{
    public class TimerMenu
    {
        TimerService _timerService = new TimerService();
        public async Task ExecuteTask()
        {
            Console.WriteLine("\nTimer was started!\n");
            var taskId = await _timerService.MarkRandomTaskWithDelay(1000);
            Console.WriteLine($"Task id: {taskId}");
        }
    }
}
