﻿using Common.DTO;
using Common.DTO.Team;
using HW4.ConsoleTeam.Services;
using System;
using System.Threading.Tasks;

namespace HW4.ConsoleTeam.ConsoleMenu
{
    class TeamMenu
    {
        private readonly TeamClientService _teamService;
        public TeamMenu()
        {
            _teamService = new TeamClientService();
        }
        public void Display ()
        {
            Console.WriteLine("\n\n\tTeams:\n");
            Console.WriteLine("1. Get all.\n" +
                "2. Get team by id.\n" +
                "3. Update team.\n" +
                "4. Delete team.\n" +
                "5. Add team.\n");
        }

        public async Task ExecuteTask()
        {
            Console.Write(" Write number: ");
            int answer = Int32.Parse(Console.ReadLine());
            switch (answer)
            {
                case 1:
                    await GetAllTeams();
                    break;
                case 2:
                    await GetTeamById();
                    break;
                case 3:
                    await UpdateTeam();
                    break;
                case 4:
                    await DeleteTeam();
                    break;
                case 5:
                    await AddNewTeam();
                    break;
                default:
                    break;
            }
        }

        private async Task GetAllTeams()
        {
            Console.WriteLine("\n\nAll teams:\n\n");
            var allTeams = await _teamService.GetAll();
            foreach (var item in allTeams)
            {
                Console.WriteLine($"Id: {item.Id}, name: {item.Name}");
            }
        }

        private async Task GetTeamById()
        {
            try
            {
                Console.Write("\n\nWrite id: ");
                int teamId = Int32.Parse(Console.ReadLine());
                var team = await _teamService.GetById(teamId);
                Console.WriteLine($"Your team is {team.Id}. {team.Name}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task UpdateTeam()
        {
            try
            {
                Console.Write("\n\nWrite team id: ");
                int teamId = Int32.Parse(Console.ReadLine());
                Console.Write("\nWrite name: ");
                string teamName = Console.ReadLine();
                var teamDto = new TeamDTO { Id = teamId, Name = teamName };
                var team = await _teamService.Update(teamDto);
                Console.WriteLine($"Team was sucessfully updated! Team id: {team.Id}. {team.Name}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task DeleteTeam()
        {
            try
            {
                Console.Write("\n\nWrite id: ");
                int teamId = Int32.Parse(Console.ReadLine());
                await _teamService.Delete(teamId);
                Console.WriteLine($"Team with id: {teamId} was sucessfully deleted!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task AddNewTeam()
        {
            try
            {
                Console.Write("\nWrite name: ");
                var teamName = Console.ReadLine();
                var teamDto = new TeamCreatedDTO { Name = teamName };
                var team = await _teamService.Create(teamDto);
                Console.WriteLine($"Team with id {team.Id} and name {team.Name} was sucessfully posted!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
